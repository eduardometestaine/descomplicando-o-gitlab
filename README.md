# Descomplicando o Gitlab

Treinamento Descomplicando o Gitlab criado ao vivo na Twitch

### Day 1

```bash
- Entendemos o que é o Git
- Entendemos o que é Working Dir, Index e HEAD
- Entendemos o que é o Gitlab
- Como criar um grupo no Gitlab
- Como criar um repositório no Gitlab
- Aprendemos comandos básicos para manipulação de arquivos e diretórios no Git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o merge na Main
```
